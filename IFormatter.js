// проверка валидности формы
$('#inputTaskTitle').on('input', function () {
    if ($('#inputTaskTitle').val() != "") {
      $('#createTaskButton').prop('disabled', false);
      $('#inputTaskTitle').removeClass('is-invalid');
      $('#inputTaskTitle').addClass('is-valid');
    } else {
      $('#createTaskButton').prop('disabled', true);
      $('#inputTaskTitle').removeClass('is-valid');
      $('#inputTaskTitle').addClass('is-invalid');
    }
});