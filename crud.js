var tasks = [];
var redux = {};
var currentProject = 0;


// пробегаемся по всем полученным таскам и чпокаем им комментарии
// for(let i = 0; i < tasks.length; i++) {
//     $.ajax({
//         url: url + `task.commentitem.getlist?TASKID=${tasks[i].id}`,
//         method: 'GET',
//         dataType: 'json',
//         async: false,
//         success: function(data){
//             tasks[i].comments = data.result;
//         },
//         error: function () {
//             Swal.fire({
//                 icon: 'error',
//                 title: 'Упс...',
//                 text: 'Не удалось получить комментарии с Битрикса!',
//                 // footer: '<a href="">Why do I have this issue?</a>'
//             });
//         }
//     });
// }

// создание задачи
function decCreateTask() {
    // непосредственно создаём задачу
    createTask(
        $('#inputTaskTitle').val(), 
        $('#inputTaskDescription').val(), 
        currentProject
    );

    // скрываем модалку создания задачи
    $('#createTask').modal('hide');

    // получаем задачи
    tasks = getTasks(currentProject);

    // сначала чистим доску от старых задач
    const cards = document.querySelectorAll('.removable');
    cards.forEach(card => {
        card.remove();
    });

    // непосредственное распределение задача по колонкам
    for(let i = 0; i < tasks.length; i++) {
        $(`#${tasks[i].status}`).append(`
        <div class="removable card draggable shadow-sm" id="cd${tasks[i].id}" draggable="true" ondragstart="drag(event)">
            <div class="card-body p-2">
                <div class="card-title">
                    <p class="fw-bold">${tasks[i].title}</p>
                </div>
                <div class="task-description">
                    ${tasks[i].description}
                </div>
                <button type="button" onclick="fill(${tasks[i].id})" class="btn btn-primary btn-sm my-2" data-bs-toggle="modal" data-bs-target="#taskView">Подробнее</button>
            </div>
        </div>
        <div class="removable dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>`)
    }
}

// заполнение расширенной версии карточки
function fill(id) {
    console.log(tasks);
    const task = tasks.find(el => el.id == id);
    $('#taskTitle').text(task.title);
    $('#taskDescription').text(task.description);
    $('#taskCreatorName').text(task.creatorName);
    $('#taskResponsibleName').text(task.responsibleName);
    $('#taskCreatedDate').text(task.createdDate);
    $('#taskEndDatePlan').text(task.endDatePlan);
    $('#taskStatus').text(task.statusText);
}

// распределение задач по колонкам
function distributeTasks(projectId, projectName) {

    // получаем задачи
    tasks = getTasks(projectId);

    // сначала чистим доску от старых задач
    const cards = document.querySelectorAll('.removable');
    cards.forEach(card => {
        card.remove();
    });


    // непосредственное распределение задача по колонкам
    for(let i = 0; i < tasks.length; i++) {
        $(`#${tasks[i].status}`).append(`
        <div class="removable card draggable shadow-sm" id="cd${tasks[i].id}" draggable="true" ondragstart="drag(event)">
            <div class="card-body p-2">
                <div class="card-title">
                    <p class="fw-bold">${tasks[i].title}</p>
                </div>
                <div class="task-description">
                    ${tasks[i].description}
                </div>
                <button type="button" onclick="fill(${tasks[i].id})" class="btn btn-primary btn-sm my-2" data-bs-toggle="modal" data-bs-target="#taskView">Подробнее</button>
            </div>
        </div>
        <div class="removable dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>`)
    }

    // закрываем модалку выбора проекта
    $('#projectsView').modal('hide');

    // надпись в навбаре
    $('#navbarProjectTitle').text(projectName);

    // в глобальную переменную
    currentProject = projectId;
}


// когда дом загрузился
$(function() {

    // загружаем проекты
    var projects = getProjects();
    console.log('Проекты:');
    console.log(projects);

    // наполняем модалку проектами
    for(let i = 0; i < 50; i++) {
        $('#projectsList').append(`<button type="button" onclick="distributeTasks(${projects[i].id}, '${projects[i].name}')" class="btn btn-primary mx-1 my-1">${projects[i].name}</button><br>`);
    }

    // предлагаем выбрать проект
    $('#projectsView').modal('show');
});






