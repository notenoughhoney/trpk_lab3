function getProjects() {
    var projects = [];
    $.ajax({
        url: url + `sonet_group.get.json`,
        method: 'GET',
        dataType: 'json',
        async: false,
        success: function(data){
            for (let i = 0; i < data.result.length; i++) {
                projects.push({
                    "id": data.result[i].ID,
                    "name": data.result[i].NAME,
                });
            }
        },
        error: function () {
            Swal.fire({
                icon: 'error',
                title: 'Упс...',
                text: 'Не удалось получить проекты с Битрикса!',
                // footer: '<a href="">Why do I have this issue?</a>'
            });
        }
    });
    return projects;
}

function getTasks(projectId) {
    var tasks = [];
    $.ajax({
        url: url + `tasks.task.list.json?filter%5BGROUP_ID%5D=${projectId}`,
        method: 'GET',
        dataType: 'json',
        async: false,
        success: function(data){
            console.log(data);
            for (let i = 0; i < data.result.tasks.length; i++) {
                var statusText = '';
                if (data.result.tasks[i].status == 2) statusText = 'Ждёт выполнения';
                if (data.result.tasks[i].status == 3) statusText = 'Выполняется'; 
                if (data.result.tasks[i].status == 4) statusText = 'Ожидает контроля';
                if (data.result.tasks[i].status == 5) statusText = 'Завершена';
                if (data.result.tasks[i].status == 6) statusText = 'Отложена';
                tasks.push({
                    "id": data.result.tasks[i].id,
                    "title": data.result.tasks[i].title,
                    "status": data.result.tasks[i].status,
                    "description": bbcodeParser.bbcodeToHtml(removeAllTags(data.result.tasks[i].description)),
                    "projectId": data.result.tasks[i].groupId,
                    "creatorName": data.result.tasks[i].creator.name,
                    "creatorIcon": data.result.tasks[i].creator.icon,
                    "responsibleName": data.result.tasks[i].responsible.name,
                    "responsibleIcon": data.result.tasks[i].responsible.icon,
                    "createdDate": data.result.tasks[i].createdDate.substring(0, 10),
                    "endDatePlan": (data.result.tasks[i].endDatePlan != null) ? data.result.tasks[i].endDatePlan : 'Не назначено',
                    "statusText": statusText,
                });
            }
        },
        error: function () {
            Swal.fire({
                icon: 'error',
                title: 'Упс...',
                text: 'Не удалось получить задачи с Битрикса!',
                // footer: '<a href="">Why do I have this issue?</a>'
            });
        }
    });    
    return tasks;
}

function updateTask(taskId, status) {
    $.ajax({
        url: url + `tasks.task.update?taskId=${taskId}&fields%5BSTATUS%5D=${status}`,
        method: 'GET',
        dataType: 'json',
        async: true,
        success: function(data){
            console.log(data);
        },
        error: function () {
            Swal.fire({
                icon: 'error',
                title: 'Упс...',
                text: 'Не удалось обновить статус задачи!',
                // footer: '<a href="">Why do I have this issue?</a>'
            });
        }
    });
}

function createTask(taskTitle, taskDescription, projectId) {
    $.ajax({
        url: url + `tasks.task.add?fields%5BTITLE%5D=${taskTitle}&fields%5BDESCRIPTION%5D=${taskDescription}&fields%5BRESPONSIBLE_ID%5D=609&fields%5BGROUP_ID%5D=${projectId}`,
        method: 'GET',
        dataType: 'json',
        async: false,
        success: function(data){
            console.log(data);
        },
        error: function () {
            Swal.fire({
                icon: 'error',
                title: 'Упс...',
                text: 'Не удалось создать задачу!',
                // footer: '<a href="">Why do I have this issue?</a>'
            });
        }
    });
}